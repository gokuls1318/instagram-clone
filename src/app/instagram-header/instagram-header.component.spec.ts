import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstagramHeaderComponent } from './instagram-header.component';

describe('InstagramHeaderComponent', () => {
  let component: InstagramHeaderComponent;
  let fixture: ComponentFixture<InstagramHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstagramHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstagramHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
