import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {

  onlinefriends=[
    {
     "friend":"assets/messi.jpg",
     "name":"suresh",
     
   },
   {
     "friend":"assets/cr7.jpg",
     "name":"ramesh"
   },
   {
     "friend":"assets/messi.jpg",
     "name":"Khan-Ameer-Aslam"
   },
   {
     "friend":"assets/cr7.jpg",
     "name":"Ameer"
   }, 
   {
     "friend":"assets/cr7.jpg",
     "name":"Jahadish"
   },
   {
     "friend":"assets/cr7.jpg",
     "name":"Gokul's"
   },
   {
     "friend":"assets/cr7.jpg",
     "name":"thiruganasambandam"
   },
   {
    "friend":"assets/cr7.jpg",
    "name":"Ameer"
  }, 
  {
    "friend":"assets/cr7.jpg",
    "name":"Jahadish"
  },
  {
    "friend":"assets/cr7.jpg",
    "name":"Gokul's"
  },
  {
    "friend":"assets/cr7.jpg",
    "name":"Ameer"
  }, 
  {
    "friend":"assets/cr7.jpg",
    "name":"Jahadish"
  },
  {
    "friend":"assets/cr7.jpg",
    "name":"Gokul's"
  },
  {
    "friend":"assets/cr7.jpg",
    "name":"Ameer"
  }, 
  {
    "friend":"assets/cr7.jpg",
    "name":"Jahadish"
  },
  {
    "friend":"assets/cr7.jpg",
    "name":"Gokul's"
  },
   
  ]
  post=[{
    "name":"Gowtham",
    "image": "assets/cr7.jpg",
    "text1":"Galatta media",
    "text2":"How Many Love Proposals Till Now? - Bharathi Kannamma Roshni Haripriyan Interview",
    "video":"assets/video2.mp4",
    "likes":"882",
    "shares":"18",
    "comments":"2",
    "views":"45k",
    "type":1,
    "location":"ooty,Tamilnadu",
    "time": "19 hours ago"
  
    },
    {
      "name":"Gow",
      "image": "assets/cr7.jpg",
      "time":"3h",
      "text1":"Ronaldo",
      "location":"ooty,Tamilnadu",
      "text2":"How Many Love Proposals Till Now? - Bharathi Kannamma Roshni Haripriyan Interview",
      "video":"assets/f.mp4",
      "pic":"assets/cr7.jpg",
      "likes":"100",
      "shares":"1",
      "comments":"1",
      "views":"5k",
      "type":2
    
    },
  ]
  follow=[{
    "image":"assets/cr7.jpg",
    "friendname":"Gokul's",
    "followedby": "Followed by Dhatchina + 4more",
  },
  {
    "image":"assets/cr7.jpg",
    "friendname":"Gowtham",
    "followedby": "Followed by Gokul's + 9more",
  },
  {
    "image":"assets/cr7.jpg",
    "friendname":"Dhatchina",
    "followedby": "Followed by vimal + 14more",
  },
  {
    "image":"assets/cr7.jpg",
    "friendname":"Ramesh",
    "followedby": "Followed by Rajesh + 10more",
  },
  {
    "image":"assets/cr7.jpg",
    "friendname":"vimalraj",
    "followedby": "Followed by Kamlesh + 6more",
  },
]
  profileobj=  {
    "Name":"gokuls",
    "Myimage":"assets/cr7.jpg",
    "username":"gokuls1995"
    
}
   

  constructor() { }

  ngOnInit(): void {
  }

}
